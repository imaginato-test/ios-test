//
//  ViewController.swift
//  imaginatoApp
//
//  Created by Shubham Chikani on 09/12/20.
//  Copyright © 2020 Shubham. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
 @IBOutlet weak var loginView: LoginView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.loginView.VC = self
        // Do any additional setup after loading the view.
    }
    
     override func viewWillAppear(_ animated: Bool) {
        self.view.bringSubviewToFront(self.loginView)
        // LoginView Lode
        self.loginView.setupShdowInView()
    }
}

