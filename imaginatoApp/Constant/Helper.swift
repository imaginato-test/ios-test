//
//  Helper.swift
//  imaginatoApp
//
//  Created by Shubham Chikani on 09/12/20.
//  Copyright © 2020 Shubham. All rights reserved.
//

import Foundation
import UIKit

@objc class Helper: NSObject{

    // MARK: ===================== CHECK HELPER VALUE IS BLANK OR NOT ========================== //
    class func userAlreadyExist(kUsernameKey: String) -> Bool {
        return UserDefaults.standard.object(forKey: kUsernameKey) != nil
    }

    // MARK: ================= Compile Blank String Method ================= //
    class func isBlank (String :String?) -> Bool {
        
        if let string = String {
            return string.isEmpty
        } else {
            
            return true
        }
    }
    
    // MARK: ===================== SET / GET :- STRING ========================== //
    class func setPREF(sValue: String, skye:String) {
        
        UserDefaults.standard.setValue(sValue, forKey: skye)
        UserDefaults.standard.synchronize()
    }
    
    class func getPREF(skey: String) ->  String {
        if let PREF = UserDefaults.standard.value(forKey: skey) as? String{
            return PREF
        }
        else{
            return ""
        }
    }
    
    // MARK: ===================== SET / GET :- INTEGER ========================== //
    class func setPREFint(sValue: Int, skye:String) {
        
        UserDefaults.standard.setValue(sValue, forKey: skye)
        UserDefaults.standard.synchronize()
    }
    class func getPREFint(skey: String) ->  Int {
        return UserDefaults.standard.integer(forKey:skey)
    }
    
    // MARK: ===================== SET / GET :- NS_MUTABLE_ARRAY ========================== //
    class func setPREFArrayNSMutable(sValue: NSMutableArray, skye:String) {
        
        UserDefaults.standard.setValue(sValue, forKey: skye)
        UserDefaults.standard.synchronize()
    }
    class func getPREFArrayNSMutable(skey: String) ->  NSMutableArray {
        return UserDefaults.standard.mutableArrayValue(forKey:skey)
    }
    
    // MARK: ===================== SET / GET :-  NS_ARRAY ========================== //
    class func setPREFArray(sValue: NSArray, skye:String) {
        
        UserDefaults.standard.setValue(sValue, forKey: skye)
        UserDefaults.standard.synchronize()
    }
    class func getPREFArray(skey: String) ->  NSArray {
        return UserDefaults.standard.mutableArrayValue(forKey:skey)
    }
    
    // MARK: ===================== SET / GET :-  any ========================== //
    class func setPREFAny(sValue: Any, skye:String) {
        
        UserDefaults.standard.setValue(sValue, forKey: skye)
        UserDefaults.standard.synchronize()
    }
    class func getPREFAny(skey: String) ->  Any {
        return UserDefaults.standard.mutableArrayValue(forKey:skey)
    }
    // MARK: ===================== SET / GET :-  bool ========================== //
    class func setPREFBool(sValue: Bool, skye:String) {
        
        UserDefaults.standard.setValue(sValue, forKey: skye)
        UserDefaults.standard.synchronize()
    }
    class func getPREFBool(skey: String) ->  Bool {
        if let PREF = UserDefaults.standard.value(forKey: skey) as? Bool{
            return PREF
        }
        else{
            return false
        }    }
    
    
    // MARK: ===================== SET / GET :-  NS_DICTIONARY ========================== //
    class func setDic(sValue: [String : Any], sKye:String){
        
        UserDefaults.standard.setValue(sValue, forKey: sKye)
        UserDefaults.standard.synchronize()
    }
    //Error
    class func getDic(skey: String) ->  [String : Any] {
        //return UserDefaults.standard.dictionaryWithValues(forKeys: [skey]) as NSDictionary
        return ((UserDefaults.standard.dictionary(forKey: skey))!)
    }

    
    // MARK: ===================== EMAIL_ADDRESS_VALIDATION ========================== //
    class func isValidEmail(testStr:String) -> Bool {
        let REGEX: String
        REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", REGEX).evaluate(with: testStr)
    }
    
    // MARK: ===================== PASSWORD_VALIDATION ========================== //
    class func isValidPass(testStr:String) -> Bool {
        let REGEX: String
        REGEX = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{4,}$"
        return NSPredicate(format: "SELF MATCHES %@", REGEX).evaluate(with: testStr)
    }

}
extension UIColor{
    func colorWithHexString(hexString: String, alpha:CGFloat = 1.0) -> UIColor {
        
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    
    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
}
