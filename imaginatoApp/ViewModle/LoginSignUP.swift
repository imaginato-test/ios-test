//
//  LoginSignUP.swift
//  imaginatoApp
//
//  Created by Shubham Chikani on 09/12/20.
//  Copyright © 2020 Shubham. All rights reserved.
//

import Foundation

struct LoginSignUPCloudHandler {
    
    func sendLoginRequest(dictRequestdata:[String:String],handler: @escaping (_ objUserInfo:User?)->(),errorHandler : @escaping (_ errorMessage:String?,_ statusCode:String?) -> () ){
        UserHandler.init().parseLoginData(dictRequestdata, handler: { (user) in
            handler(user)
        }) { (strErrorMsg, strErrorCode) in
            errorHandler(strErrorMsg,strErrorCode)
        }
    }
}
