//
//  LoginView.swift
//  imaginatoApp
//
//  Created by Shubham Chikani on 09/12/20.
//  Copyright © 2020 Shubham. All rights reserved.
//

import UIKit
import Toaster
import SwiftyJSON

typealias LoginSignUPSucess = (_ userModle:User?)->()
typealias failureHandler = (_ isSucess:Bool?)->()

class LoginView: UIView,UITextFieldDelegate {
    
    var view: UIView!
    weak var VC:UIViewController!

    @IBOutlet weak var btnEye: UIButton!
    @IBOutlet weak var txtFieldEmail: UITextField!
    @IBOutlet weak var txtFieldPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnLoginWithGoogle: UIButton!
    @IBOutlet weak var btnLoginFacebook: UIButton!
    
    var sucessCompleationHandler : LoginSignUPSucess!
    var failureCompleationHandler : failureHandler!
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        // Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // Setup view from .xib file
        xibSetup()
    }
    
    //MARK:
    //MARK: Setup Layout view
    func setupShdowInView(){
       self.txtFieldEmail.dropShadow(color: UIColor.init(red: 31/255.0, green: 84/255.0, blue: 195/255.0, alpha: 0.15), opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        self.txtFieldPassword.dropShadow(color: UIColor.init(red: 31/255.0, green: 84/255.0, blue: 195/255.0, alpha: 0.15), opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        
        self.txtFieldEmail.font = PopinsRegular
        self.txtFieldPassword.font = PopinsRegular
        
        self.btnLogin.titleLabel?.font = PopinsBold
        self.btnLogin.isUserInteractionEnabled = false
        self.btnLogin.backgroundColor = UIColor.gray
        self.txtFieldEmail.delegate = self
        self.txtFieldPassword.delegate = self
        
        self.btnLogin.setupdefaultCorner()
    }
    
    //MARK:
    //MARK: Buttons click events
    @IBAction func didClickBtnEye(_ sender: UIButton) {
        if txtFieldPassword.isSecureTextEntry == true{
             txtFieldPassword.isSecureTextEntry = false
            sender.isSelected = true
        }else{
            txtFieldPassword.isSecureTextEntry = true
            sender.isSelected = false
        }
    }
    @IBAction func didClickBtnSignUP(_ sender: UIButton) {
        self.view.endEditing(true)
        if isInternetAvailable == false{ // check internet is not available
            Toast(text: "Please check your internet connection").show()
            return
        }
        self.checkValidation()
    }
}

//MARK:
//MARK: xib Lode
private extension LoginView {
    func xibSetup() {
        backgroundColor = UIColor.clear
        view = loadNib()
        // use bounds not frame or it'll be offset
        view.frame = bounds
        // Adding custom subview on top of our view
        addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[childView]|",
                                                      options: [],
                                                      metrics: nil,
                                                      views: ["childView": view!]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[childView]|",
                                                      options: [],
                                                      metrics: nil,
                                                      views: ["childView": view!]))
        
        self.btnLogin.setupdefaultCorner()
    }
    
    //MARK:
    //MARK: CheckValidation
    func checkValidation()->Bool{
        if txtFieldEmail.text == ""{
            Toast(text: "Please enter email").show()
        }
        else if(Helper.isValidEmail(testStr:self.txtFieldEmail.text!) == false){
         Toast(text: "This is a invalid email").show()
         return false
        }
        else if txtFieldPassword.text == ""{
            Toast(text: "Please enter password").show()
        }
        else if(Helper.isValidPass(testStr:self.txtFieldPassword.text!) == false){
            Toast(text: "Passwords require at least 1 up percase, 1 lowercase, and 1 number").show()
            return false
        }
        else
        {
            let dictRequest = ["email":txtFieldEmail.text!,"password":txtFieldPassword.text!]
            //(VC as! ViewController).showProgressForLogin()
            LoginSignUPCloudHandler.init().sendLoginRequest(dictRequestdata: dictRequest, handler: { (user) in
                if  self.sucessCompleationHandler != nil{
                    self.sucessCompleationHandler(user)
                }
            }, errorHandler: { (strErrorMsg, strErrorCode) in
                if  self.failureCompleationHandler != nil{
                    self.failureCompleationHandler(true)
                }
                Toast(text: strErrorMsg).show()
            })
        }
         return true
    }
    
    //MARK:
    //MARK: ButtonCheckValidation
    func ButtoncheckValidation()->Bool{
        if txtFieldEmail.text == ""{
            btnLogin.isUserInteractionEnabled = false
            self.btnLogin.backgroundColor = UIColor.gray
        }
        else if(Helper.isValidEmail(testStr:self.txtFieldEmail.text!) == false){
         btnLogin.isUserInteractionEnabled = false
         self.btnLogin.backgroundColor = UIColor.gray
         return false
        }
        else if txtFieldPassword.text == ""{
            btnLogin.isUserInteractionEnabled = false
            self.btnLogin.backgroundColor = UIColor.gray
        }
        else if(Helper.isValidPass(testStr:self.txtFieldPassword.text!) == false){
            btnLogin.isUserInteractionEnabled = false
            self.btnLogin.backgroundColor = UIColor.gray
            return false
        }
        else
        {
            btnLogin.isUserInteractionEnabled = true
            self.btnLogin.backgroundColor = UIColor.green
        }
         return true
    }

}


//MARK:
//MARK: textField Delegate method
extension LoginView{
    func textFieldDidBeginEditing(_ textField: UITextField) {
     
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.ButtoncheckValidation()
        if string == " " && range.location == 0{
            return false
        }
        if textField == txtFieldPassword{
            if string == " "{
                return false
            }
        }
        return true
    }
}
@IBDesignable
extension UIButton{

    func setupdefaultCorner(){
        self.layer.cornerRadius = 10
        self.clipsToBounds = true
    }
}
