//
//  User.swift
//  imaginatoApp
//
//  Created by Shubham Chikani on 09/12/20.
//  Copyright © 2020 Shubham. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Toaster

struct User {
    var strUserID:String! = ""
    var strUserName:String! = ""
    var strCreatedat:String! = ""
    
}

struct UserHandler {
    func  parseLoginData(_ dictRequestParameter:[String:String] , handler: @escaping (_ objUserInfo:User?)->(), errorHandler : @escaping (_ errorMessage:String?,_ statusCode:String?) -> ()) {
        ServiceHandler.init().requestForlogin(dictRequestParam: dictRequestParameter, compleationHandler: { (json) in
            print(json)
            if let userJson1 = json.dictionaryValue["data"]{
              if let userJson = userJson1.dictionaryValue["user"]{
                let objUser = User.init(strUserID: userJson.dictionaryValue["userId"]?.stringValue ?? "", strUserName: userJson.dictionaryValue["userName"]?.stringValue ?? "", strCreatedat: userJson.dictionaryValue["created_at"]?.stringValue ?? "")
                handler(objUser)
              }
            }
        }) { (error,errorCode) in
           errorHandler(error,errorCode)
        }
    }
}
