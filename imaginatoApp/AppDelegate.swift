//
//  AppDelegate.swift
//  imaginatoApp
//
//  Created by Shubham Chikani on 09/12/20.
//  Copyright © 2020 Shubham. All rights reserved.
//

import UIKit
import IQKeyboardManager
import Alamofire
import Toaster
var isInternetAvailable:Bool = true

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    let net = NetworkReachabilityManager()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.shared().isEnabled = true
        // Override point for customization after application launch.
        net?.listener = { status in
            if self.net?.isReachable ?? false {
                
                switch status {
                    
                case .reachable(.ethernetOrWiFi):
                    isInternetAvailable = true
                    //Toast.init(text: "The network is reachable over the WiFi connection").show()
                    print("The network is reachable over the WiFi connection")
                case .reachable(.wwan):
                    isInternetAvailable = true
                    //Toast.init(text: "The network is reachable over the WWAN connection").show()
                    print("The network is reachable over the WWAN connection")
                case .notReachable:
                    isInternetAvailable = false
                     Toast.init(text: "The network is not reachable").show()
                    print("The network is not reachable")
                case .unknown :
                    isInternetAvailable = false
                     Toast.init(text: "It is unknown whether the network is reachable").show()
                    print("It is unknown whether the network is reachable")
                }
            }else{
                isInternetAvailable = self.net?.isReachable ?? false
                  Toast.init(text: "The network is not reachable").show()
            }
        }
            net?.startListening()
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

