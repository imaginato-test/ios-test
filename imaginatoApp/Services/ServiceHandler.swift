//
//  ServiceHandler.swift
//  imaginatoApp
//
//  Created by Shubham Chikani on 09/12/20.
//  Copyright © 2020 Shubham. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import Toaster


struct  ServiceHandler {
    
    func requestForlogin(dictRequestParam:[String:String], compleationHandler : @escaping (_ json:JSON)->(), errorHandler : @escaping (_ errorMessage:String?,_ statusCode:String?)->()){
        print(dictRequestParam)
        let data = try! JSONSerialization.data(withJSONObject: dictRequestParam, options: JSONSerialization.WritingOptions.prettyPrinted)

        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        print(json!)
        let baseUrl = ServiceURL+APIName.login
        let urlComponent = URLComponents(string: baseUrl)!
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = "POST"
        request.httpBody = json!.data(using: String.Encoding.utf8.rawValue);
        Alamofire.request(request).responseJSON { (responce) in
            print(responce)
            if responce.response?.statusCode == 200{
                let objJson = JSON.init(responce.result.value!)
                if objJson.dictionaryValue["result"]?.stringValue == "1"{
                     compleationHandler(objJson)
                      Toast(text: "User Successfully Login").show()
                }else{
                     errorHandler(objJson.dictionaryValue["error_message"]?.stringValue,objJson.dictionaryValue["result"]?.stringValue)
                }
            }else{
                if let value = responce.result.value{
                     let objJson = JSON.init(value)
                     errorHandler(objJson.dictionaryValue["message"]?.stringValue, objJson.dictionaryValue["response_code"]?.stringValue)
                }
                else{
                    errorHandler("Something went worng please contact "+APIName().contactSupportEmail,String(responce.response!.statusCode ))
                }
            }
        }
    }
    
}
