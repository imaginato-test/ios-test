//
//  StringExtension.swift
//  imaginatoApp
//
//  Created by Shubham Chikani on 09/12/20.
//  Copyright © 2020 Shubham. All rights reserved.
//

import Foundation

extension String{
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: self)
    }
    
    func isValidePassword() -> Bool {
        let emailRegEx = "[A-Za-z0-9!@#$%^&*({}):\"<>?.,]{8,15}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: self)
    }
}
